#!/usr/bin/env python3
import sys
import os
import cv2
import argparse
import math
import logging as log
import annotation_tools.utils as utils

FILE_IDX = 0

def is_file(parser, x) :
    """
    Checks whether a file exists.

    Args:
        parser: ArgumentParser object
        x: Filename
    Returns:
        Filename if exists.
    """
    if not os.path.isfile(x) :
        parser.error("Input video file %s does not exist!!!" %x)
    return x

def is_directory(parser, x) :
    """
    Checks whether a directory exists.

    Args:
        parser: ArgumentParser object.
        x: Directory name.
    Returns:
        Directory name if exists.
    """
    if not os.path.isdir(x) :
        parser.error("Input directory %s does not exist!!!" %x)
    return x

def parse_arguments():
    """
    Argument parser.

    Returns:
        ArgumentParser object
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--video", help="Video file to process", type=lambda x: is_file(parser, x))
    parser.add_argument("-d", "--directory", help="Directory of video files to process", type=lambda x: is_directory(parser, x))
    parser.add_argument("-o", "--output", help="Directory path to store the images", required=False, default="images")
    parser.add_argument("-i", "--index", help="ID of the first image. Will be increased by one for each frame", type=int, default=0)

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-s", "--seconds", help="Interval between saving frames in seconds", type=int)
    group.add_argument("-f", "--frequency", help="Saving will be done once every x frames", type=int)

    return parser.parse_args()

def save_frames(args, video_file):
    """
    Saves frames from the video file based on the given arguments.

    Args:
        args: Command line arguments.
        video_file: Name of the video file.
    """
    global FILE_IDX
    cap = cv2.VideoCapture(video_file)
    frame_rate = cap.get(cv2.CAP_PROP_FPS)
    count = math.floor(frame_rate)
    if args.seconds:
        count *= args.seconds
    elif args.frequency:
        count = args.frequency
    output_directory = args.output

    while(cap.isOpened()):
        frame_id = cap.get(cv2.CAP_PROP_POS_FRAMES)
        ret, frame = cap.read()
        if (True != ret):
            break
        if (0 == frame_id % count):
            n = os.path.splitext(os.path.basename(video_file))[0]
            log.info("Saving frame '" + str(int(frame_id)) + "' of video '" + video_file + "'.")
            filename = output_directory + "/" + str(FILE_IDX) + ".jpg"
            cv2.imwrite(filename, frame)
            FILE_IDX += 1
    cap.release()

def main():
    """
    Collects the frames from the video files based on the given arguments.
    Frames will be saved once every second if non of arguments '-s' and '-f' are specified.
    """
    global FILE_IDX
    args = parse_arguments()
    try:
        if not (args.video or args.directory):
            raise Exception('No action requested, add --video or --directory or both.')

        video_files = []
        if args.video and utils.is_video(args.video):
            video_files.append(args.video) 
        if args.directory:
            for f in os.listdir(args.directory):
                if utils.is_video(f):
                    video_files.append(os.path.join(args.directory, f))
        if (0 == len(video_files)):
            raise Exception("No video file(s) is(are) specified.")
        
        utils.create_directory(args.output)
        FILE_IDX = args.index
        for video_file in video_files:
            save_frames(args, video_file)

    except Exception as err:
        log.error(str(err))

if __name__ == "__main__":
    log.basicConfig(stream=sys.stdout, level=log.INFO)
    main()
