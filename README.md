# Annotation Toolkit

This repository contains a collection of tools for editing and creating [COCO style datasets](http://cocodataset.org/#download).

## Capabilities:
* Load and visualize a COCO style dataset
* Edit Class Labels
* Edit Bounding Boxes
* Edit Keypoints
* Export a COCO style dataet
* Bounding Box Tasks for Amazon Mechanical Turk

## Not Implemented:
* Edit Segmentations
* Keypoint tasks for Amazon Mechanical Turk
* Class label tasks for Amazon Mechanical Turk
* Segmentation tasks for Amazon Mechanical Turk

# Requirements, Environments and Installation
- Make sure [MongoDB](https://www.mongodb.com/) is [installed](https://docs.mongodb.com/manual/installation/#tutorials) and [running](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/#run-mongodb). E.g. for Ubuntu 16.04 see [here](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/#import-the-public-key-used-by-the-package-management-system)).

- Install [OpenCV](https://opencv.org) library from sources with [ffmpeg](https://www.ffmpeg.org/download.html) support.

- The tools are primarily tested using the [Chrome web browser](https://www.google.com/chrome/browser/desktop/index.html).

- Install the python dependencies:
```
$ pip install -r requirements.txt
```

## Labeling
The job consists of the two main procedures:
- Collecting images.
- Labeling images.

### Collecting images
All data (images) can be collected by saving specific frames from the video files.   
Use a script `save_frames.py` for collecting and storing the frames from the provided video file.
```
./save_frames.py -v <video_filename> -d <directory_path> -o <output_directory>
```
Argument list:
***
    * -v, --video <video_filename>
        Type: String
        Description: Name of the video file
    * -d, --directory <directory_path>
        Type: String
        Description: Path to a directory containing video file(s)
    * -o, --output <output_directory>
        Type: String
        Description: Name of the output directory to store the extracted frames from the video file(s)
    * -i, --index <idx>
        Type: Integer
        Description: ID of the first image. Will be increased by one for each frame. Default is 0.

The argument `-o` and at least one of the `-v` and `-d` arguments are mandatory.   
By default the frames will be saved once every second.
This behaviour can be changed by passing the just one of the arguments mentioned below:
***
    * -f, --frequency <frequency>
        Type: Integer
        Description: Frames will be saved once every `X` frame
    * -s, --seconds <seconds>
        Type: Integer
        Description: Interval between saving frames in seconds

NOTE: `-f` and `-s` options are mutually exclusive and cannot be used together.

Example:
```
./save_frames.py -d videos -o output_dir -s 5
```

## Labeling images
Once the images are extracted from the video file the following steps should be done for annotation.

1. Host images locally:
```
python -m SimpleHTTPServer 8007
```

2. Run MongoDB [link](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/#run-mongodb):
```
mkdir -p data/db
mongod --dbpath data/db/
```

3. Load Dataset
```
python3 load.py <path/to/directory/of/images/>
```

4. Start the annotation tool web server:
```
python3 run.py --port 8008
```

5. Open the browser and type the following line.
```
http://localhost:8008/edit_task/?start=0&end=<image_end_id>&category_id=1
```
Where `<image_end_id>` number of images which should be annotated.   
Remove the existing dummy annotation (Press button `Delete`), then create new annotation (Press button `New`).
Manually annotate the image (`bounding boxes`, `keypoints`, etc.) and press the `Save` buttone to save the annotations. 

6. Export Dataset
```
python3 export.py <path/to/annotated/json/file>
```
The `<path/to/final/json/file>` file is ready to feed to the NN.
