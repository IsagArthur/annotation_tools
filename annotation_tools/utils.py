"""
Generic utilities.
"""

COLOR_LIST = [
  "#e6194b",  # red
  "#3cb44b",  # green
  "#ffe119",  # yellow
  "#0082c8",  # blue
  "#f58231",  # orange
  "#911eb4",  # purple
  "#46f0f0",  # cyan
  "#f032e6",  # magenta
  "#d2f53c",  # lime
  "#fabebe",  # pink
  "#008080",  # teal
  "#e6beff",  # lavender
  "#aa6e28",  # brown
  "#fffac8",  # beige
  "#800000",  # maroon
  "#aaffc3",  # mint
  "#808000",  # olive
  "#ffd8b1",  # coral
  "#000080",  # navy
  "#808080",  # grey
  "#FFFFFF",  # white
  "#000000"   # black
]

import json
import cv2
import os

def import_dict_from_json(dict_json_filename):
    """
    Loads given JSON file into a python dict object.

    Args:
        dict_json_filename: Path to input JSON file.
    Returns:
        Dict object
    """
    data = {}
    with open(dict_json_filename, 'r') as f:
        data = json.load(f)
    return data

def is_image(input_path): 
    """     
    Returns true if the given file is an image.
        
    Args:
        input_path: Path to input file.
    Returns:
        True if the given file is an image.
    """ 
    return input_path.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.tif', '.tiff'))

def is_video(input_path):
    """
    Returns true if the given file is a video.

    Args:
        input_path: Path to input file.
    Returns:
        True if the given file is a video.
    """
    return input_path.lower().endswith(('.mov', '.avi', '.mp4'))

def get_image_size(img_path):
    """     
    Returns the size of the given image file.
        
    Args:
        img_path: Path to image file.
    Returns:
        The size of the image.
    """ 
    img = cv2.imread(img_path)
    return img.shape[:2]

def get_json_field(json_object, field_name):
    """
    Gets the value of the given field for JSON object.
    
    Args:
        json_object: JSON object.
        field_name: Name of the field.
    Returns:
        The value of the given field.
    """
    if field_name not in json_object:
        raise Exception("'" + field_name + "' field is missing in input JSON file!")
    return json_object[field_name]

def create_directory(dir_path):
    """
    Create a directory if it does not exist.

    Args:
        dir_path: Create a directory to save the resources.
    """
    if os.path.exists(dir_path):
        raise Exception("Directory '" + str(dir_path) +
                                   "' to save the resources already exists.")
    os.makedirs(dir_path)

