#!/usr/bin/env python3
import sys
import json
import os
import cv2
from time import strftime
import logging as log
from sortedcontainers import SortedSet
import logging as log
from . import utils

ANNOTATION_ID = 0

def fix_image(image, new_images, not_annotated_images):
    """
    Fix images field.

    Args:
        image: Single image.
        new_images: New list to store fixed images.
        not_annotated_images: Stores not annotated or incorrectly annotated images.
    """
    image_id = int(image["id"])
    if image_id in not_annotated_images:
        log.info("Image, with ID '" + str(image_id) + "' is not annotated or annotated incorrectly.")
        return

    dict_obj = {}
    dict_obj["file_name"] = image["file_name"]
    dict_obj["id"] = image_id
    dict_obj["height"] = image["height"]
    dict_obj["width"] = image["width"]
    dict_obj["url"] = image["url"]
    dict_obj["rights_holder"] = image["rights_holder"]
    dict_obj["license"] = int(image["license"])
    new_images.append(dict_obj)

def fix_annotation(annotation, new_annotations, not_annotated_images):
    """
    Fix annotations field.

    Args:
        annotation: Single annotation.
        new_annotations: List to store fixed annotations.
        not_annotated_images: Set to store not annotated or incorrectly annotated images.
    """
    global ANNOTATION_ID
    bbox = annotation["bbox"]
    keypoints = annotation["keypoints"]
    image_id = int(annotation["image_id"])
    dist = annotation.get("dist")
    pose = annotation.get("pose")
    if 57 != len(keypoints) or all(b == 0 for b in bbox):
        not_annotated_images.add(image_id)
        return
    if image_id in not_annotated_images:
        not_annotated_images.remove(image_id)

    dict_obj = {}
    dict_obj["bbox"] = bbox
    dict_obj["category_id"] = int(annotation["category_id"])
    dict_obj["id"] = ANNOTATION_ID
    dict_obj["image_id"] = image_id
    dict_obj["keypoints"] = keypoints
    dict_obj["num_keypoints"] = len(keypoints) // 3
    dict_obj["distance"] = dist
    dict_obj["pose"] = pose
    new_annotations.append(dict_obj)
    ANNOTATION_ID += 1

def fix_category(category, new_categories):
    """
    Fix categories field.

    Args:
        category: Single category.
        new_categories: List to store fixed categories.
    """
    dict_obj = {}
    dict_obj["keypoints"] = category["keypoints"]
    dict_obj["name"] = category["name"]
    dict_obj["skeleton"] = category["skeleton"]
    dict_obj["id"] = int(category["id"])
    dict_obj["supercategory"] = category["supercategory"]
    dict_obj["keypoints_style"] = category["keypoints_style"]
    new_categories.append(dict_obj)

def fix_license(license, new_licenses):
    """
    Fix licenses field.

    Args:
        license: Single license.
        new_categories: List to store fixed licenses.
    """
    dict_obj = {}
    dict_obj["id"] = int(license["id"])
    dict_obj["name"] = license["name"]
    dict_obj["url"] = license["url"]
    new_licenses.append(dict_obj)

def fix_annotations(input_json, output_json):
    """
    Fix the fields in input JSON file to satisfy COCO dataset format.

    Args:
        input_json: Input json file.
        output_json: Output JSON file in COCO format.
    """
    output_dict = {}
    json_object = utils.import_dict_from_json(input_json)

    not_annotated_images = SortedSet()
    annotations = utils.get_json_field(json_object, "annotations")
    new_annotations = []
    for annotation in annotations:
        fix_annotation(annotation, new_annotations, not_annotated_images)

    images = utils.get_json_field(json_object, "images")
    new_images = []
    for image in images:
        image_dict = fix_image(image, new_images, not_annotated_images)

    categories = utils.get_json_field(json_object, "categories")
    new_categories = []
    for category in categories:
        category_dict = fix_category(category, new_categories)

    licenses = utils.get_json_field(json_object, "licenses")
    new_licenses = []
    for license in licenses:
        license_dict = fix_license(license, new_licenses)

    output_dict["annotations"] = new_annotations
    output_dict["images"] = new_images
    output_dict["categories"] = new_categories
    output_dict["licenses"] = new_licenses
    with open(output_json, 'w') as json_file:
        json.dump(output_dict, json_file, indent=4)

if __name__ == "__main__":
    log.basicConfig(stream=sys.stdout, level=log.INFO)
    fix_annotations(sys.argv[1], sys.argv[2])
