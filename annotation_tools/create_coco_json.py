#!/usr/bin/env python3
import sys
import json
import os
from time import strftime
import logging as log
from . import utils

url_base = "http://localhost:8007/"

def get_coco_image(img_path, img_file):
    """
    Creates empty images field in COCO dataset format.

    Args:
        img_path: Path to the image file.
        img_file: Image filename.
    Returns:
        Dict object for field images.
    """
    dict_obj = {}
    dict_obj["license"] = "1"
    dict_obj["file_name"] = img_file
    height, width = utils.get_image_size(img_path)
    dict_obj["height"] = height
    dict_obj["width"] = width
    dict_obj["id"] = int(os.path.splitext(os.path.basename(img_file))[0])
    dict_obj["url"] = url_base + img_path
    dict_obj["rights_holder"] = ""
    return dict_obj

def get_coco_annotation(img_file, annotation_id):
    """
    Creates empty annotations field in COCO dataset format.

    Args:
        img_file: Image filename.
        annotation_id: Unique annotation ID.
    Returns:
        Dict object for field annotations.
    """
    dict_obj = {}
    dict_obj["num_keypoints"] = 19
    dict_obj["keypoints"] = []
    dict_obj["image_id"] = int(os.path.splitext(os.path.basename(img_file))[0])
    dict_obj["bbox"] = [0, 0, 0, 0]
    dict_obj["category_id"] = 1
    dict_obj["id"] = annotation_id
    return dict_obj

def create_coco_json(images_path, output_json):
    """
    Generats JSON in COCO dataset format with the
    empty annotations for each image from the given dataset.

    Args:
        images_path: Input directory containing the images.
        output_json: Path to the Output JSON file.
    """
    output_dict = {}
    template_json = "./annotation_tools/resources/person_template.json"
    json_object = utils.import_dict_from_json(template_json)

    categories = json_object["categories"]
    output_dict["categories"] = categories
    licenses = json_object["licenses"]
    output_dict["licenses"] = licenses

    output_dict["annotations"] = []
    output_dict["images"] = []

    image_files = []
    annotation_id = 0
    for img_file in os.listdir(images_path):
        if not utils.is_image(img_file):
            continue
        img_path = os.path.join(images_path, img_file)
        output_dict["images"].append(get_coco_image(img_path, img_file))
        output_dict["annotations"].append(get_coco_annotation(img_file, annotation_id))
        annotation_id += 1

    with open(output_json, 'w') as json_file:
        json.dump(output_dict, json_file, indent=4)

if __name__ == "__main__":
    create_coco_json(sys.argv[1], sys.argv[2])
