#!/usr/bin/env python3

import sys
import json

from annotation_tools.create_coco_json import create_coco_json
from annotation_tools.annotation_tools import get_db
from annotation_tools.db_dataset_utils import ensure_dataset_indices, load_dataset

def main(images_path):
    empty_coco_json = "empty_coco_json.json"
    create_coco_json(images_path, empty_coco_json)

    db = get_db()
    with open(empty_coco_json) as f:
        dataset = json.load(f)
    ensure_dataset_indices(db)
    load_dataset(db, dataset, True)

if __name__ == "__main__":
    main(sys.argv[1])
