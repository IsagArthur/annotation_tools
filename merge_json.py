#!/usr/bin/env python3
import sys
import json
import argparse
import logging as log

def parse_arguments():
    """
    Argument parser.

    :return: ArgumentParser object
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_jsons", help = "Input json. More then one json is required.", nargs = '+', required = True)
    parser.add_argument("-o", "--output_json", help = "Output json. Default is output.json", default = "output.json")
    return parser.parse_args()

def main():
    """
    Merges the given number of input JSON files into output JSON file.
    """
    args = parse_arguments()
    try:
        if len(args.input_jsons) < 2:
            raise Exception("More then one input JSON file is required.")

        final_data = {}
        final_data["annotations"] = []
        final_data["images"] = []
        for i in range(len(args.input_jsons)):
            file_name = args.input_jsons[i]
            data = json.load(open(file_name))
            final_data["annotations"] += data["annotations"]
            final_data["images"] += data["images"]

        file_name = args.input_jsons[0]
        data = json.load(open(file_name))
        final_data["categories"] = data["categories"]
        final_data["licenses"] = data["licenses"]

        with open(args.output_json, 'w') as json_file:
            json.dump(final_data, json_file, indent=4)

    except Exception as err:
        log.error(str(err))

if __name__ == '__main__': 
    log.basicConfig(stream=sys.stdout, level=log.INFO)
    main()
