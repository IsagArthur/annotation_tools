#!/usr/bin/env python3

import sys
import json

from annotation_tools.annotation_tools import get_db
from annotation_tools.db_dataset_utils import ensure_dataset_indices, import_dataset

def main(images_path, json_path):
    print(json_path)
    print(images_path)

    db = get_db()
    with open(json_path) as f:
        dataset = json.load(f)
    ensure_dataset_indices(db)
    import_dataset(db, dataset, images_path, True)

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
