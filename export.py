#!/usr/bin/env python3

import sys
import json
import logging

from annotation_tools.fix_annotations import fix_annotations
from annotation_tools.annotation_tools import get_db
from annotation_tools.db_dataset_utils import export_dataset

def main(json_path):
    tmp_json = "tmp_dataset.json"
    db = get_db()
    dataset = export_dataset(db, True)
    with open(tmp_json, 'w') as f:
        json.dump(dataset, f)
    fix_annotations(tmp_json, json_path)

if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    main(sys.argv[1])
